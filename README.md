# OWN I3 DOT

## How to use

```bash
git clone https://gitlab.com/ownos/own-i3-dot.git ~/.config/i3
```

## Keys

| Keybinding              | Function                         |
| ----------------------- | -------------------------------- |
| `Mod + Space`           | Open Application Launcher        |
| `Mod + Shift + Q`       | Kill Application                 |
| `Mod + X`               | Open power menu                  |
| **Applications**        |                                  |
| `Mod + Enter`           | Open Terminal                    |
| `Mod + F`               | Open File Manager                |
| `Mod + W`               | Open Browser                     |
| **Window**              |                                  |
| `Mod + Space`           | Turn floating mode               |
| `Mod + LMB`             | Move floating window             |
| `Mod + Shift + ← ↑ → ↓` | Move window                      |
| `Mod + Ctrl + ← ↑ → ↓`  | Resize window                    |
| **Workspace**           |                                  |
| `Mod + Alt + Right`     | Switch to next workspaces        |
| `Mod + Alt + Left`      | Switch to previous workspaces    |
| `Alt + 1...8`           | Switch between workspaces        |
| `Alt + Shift + 1...8`   | Move focused window to workspace |
| **Layout**              |                                  |
| `Alt + Tab`             | Turn tabbed mode                 |
| `Mod + Tab`             | Switch layout to horizontal      |
| `Mod + Shift + H`       | Toggle Split layout              |
| **Borders**             |                                  |
| `Mod + n`               | Border normal with title         |
| `Mod + u`               | Remove border                    |
| `Mod + y`               | Reset border                     |
| **Gaps**                |                                  |
| `Mod + Plus`            | Increase inner gaps              |
| `Mod + Minus`           | Decrease inner gaps              |
| `Mod + Shift + Plus`    | Increase outer gaps              |
| `Mod + Shift + Minus`   | Decrease outer gaps              |
